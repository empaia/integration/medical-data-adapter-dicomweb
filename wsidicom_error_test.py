from wsidicom import WsiDicom

wsidicom_slide = WsiDicom.open(
    "/home/dschacherer/empaia/test_files/Study_2.25.52013921648145628065553937497355021549/Series_1.3.6.1.4.1.5962.99.1.133261056.1397741034.1640810800896.2.0"
)
print("levels", wsidicom_slide.levels)
print("labels", wsidicom_slide.labels)
print("overviews", wsidicom_slide.overviews)
print("coll", wsidicom_slide.collection)

thumbnail = wsidicom_slide.read_thumbnail()
print("thumbnail", type(thumbnail))
label = wsidicom_slide.read_label()
print("label", type(label))
macro = wsidicom_slide.read_overview()
print("overview", type(macro))
