# Medical Data Adapter DICOMweb

ATTENTION: THIS IS A PROTOTYPE AND NOT READY FOR PRODUCTION USE

## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone medical-data-adapter-dicomweb

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd medical-data-adapter-dicomweb
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Access the interactive OpenAPI specification in a browser:

* http://localhost:8000/docs
* http://localhost:8000/v3/docs

Access DCM4CHEE web UI:

* http://localhost:8080/dcm4chee-arc/ui2

Send WSI files to DCM4CHEE for testing:

* https://github.com/dcm4che/dcm4che/blob/master/dcm4che-tool/dcm4che-tool-storescu/README.md

```bash
storescu -c DCM4CHEE@localhost:11112 ${PATH_TO_WSIS}/DICOM_wsidicomizer/DICOM_Aperio
storescu -c DCM4CHEE@localhost:11112 ${PATH_TO_WSIS}/DICOM_wsidicomizer/DICOM_Mirax
```

### Stop and Remove

```bash
docker-compose down
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
isort .
black .
pycodestyle medical_data_adapter_dicomweb tests
pylint medical_data_adapter_dicomweb tests
pytest --maxfail=1  # requires service running
```

