from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    disable_openapi: bool = False
    root_path: str = ""
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    debug: bool = False
    padding_color: tuple = (255, 255, 255)
    enable_viewer_routes: bool = False
    max_returned_region_size: int = 25_000_000  # e.g. 5000 x 5000
    max_thumbnail_size: int = 500
    dicom_webserver_url: str = "http://localhost"

    model_config = SettingsConfigDict(env_prefix="MDAD_", env_file=".env")
