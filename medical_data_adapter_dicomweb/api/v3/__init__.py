from .clinical_data import add_public_routes_clinical


def add_routes_v3(app, settings):
    add_public_routes_clinical(app, settings)
