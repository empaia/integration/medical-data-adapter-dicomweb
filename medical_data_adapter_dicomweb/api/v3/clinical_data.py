from typing import List

from fastapi import Body, Path, Query
from fastapi.responses import StreamingResponse
from pydantic import UUID4, conint

from medical_data_adapter_dicomweb.api.v3.clients import dicom_connector
from medical_data_adapter_dicomweb.custom_models.queries import (
    ImageChannelQuery,
    ImageFormatsQuery,
    ImagePaddingColorQuery,
    ImageQualityQuery,
    PluginQuery,
    ZStackQuery,
)
from medical_data_adapter_dicomweb.custom_models.responses import ImageRegionResponse, ImageResponses
from medical_data_adapter_dicomweb.models.v3.clinical import (
    Case,
    CaseList,
    ClinicalSlide,
    ClinicalSlideList,
    ClinicalSlideQuery,
)
from medical_data_adapter_dicomweb.models.v3.slide import SlideInfo
from medical_data_adapter_dicomweb.singletons import tags_dict


def add_public_routes_clinical(app, settings):
    @app.get(
        "/tags",
        response_model=dict,
        status_code=200,
        tags=["Default"],
        summary="Get all allowed tags",
    )
    def _() -> dict:
        """Get dictionary of allowed tags"""
        return tags_dict

    @app.get(
        "/cases/{case_id}",
        response_model=Case,
        responses={
            200: {"description": "The full Case (except Slides) if it exists"},
            400: {"description": "Case not found"},
        },
        tags=["Clinical Data"],
        summary="Get case by ID",
    )
    def _(
        case_id: UUID4 = Path(..., description="The ID of the case to retrieve"),
        with_slides: bool = False,
    ) -> Case:
        """
        Get case with the given ID, if it exists, or raise an HTTP Exception otherwise.
        The case does not include Slides; to get all Slides for a specific case, use the
        `/slides/query` route.
        * Input `case_id`: The ID of the case to retrieve
        * Output: The full Case (except Slides) if it exists
        """
        return dicom_connector.get_case(case_id=case_id, with_slides=with_slides)

    @app.get(
        "/cases",
        response_model=CaseList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Get list of all cases",
    )
    def _(
        with_slides: bool = False,
        skip: conint(ge=0) = Query(None, description="Number of cases to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> CaseList:
        """
        Get list of all Cases, newest first, with paging.
        * Input `skip`:  Number of cases to skip, for paging
        * Input `limit`: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        return dicom_connector.get_cases(with_slides=with_slides, skip=skip, limit=limit)

    @app.get(
        "/slides/{slide_id}",
        response_model=ClinicalSlide,
        responses={
            200: {"description": "The full Slide, if it exists"},
            400: {"description": "Slide not found"},
        },
        tags=["Clinical Data"],
        summary="Get slide by ID",
    )
    def _(
        slide_id: UUID4 = Path(..., description="The ID of the Slide to retrieve"),
    ) -> ClinicalSlide:
        """
        Get the Slide with the given ID, or raise an HTTP Error if it does not exist.
        * Input `slide_id`: The ID of the Slide to retrieve
        * Output: The full Slide, if it exists
        """
        return dicom_connector.get_slide(slide_id=slide_id)

    @app.get(
        "/slides",
        response_model=ClinicalSlideList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Get list of all slides",
    )
    def _(
        skip: conint(ge=0) = Query(None, description="Number of slides to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> ClinicalSlideList:
        """
        Get list of all Slides, newest first, with paging.
        * Input skip: Number of cases to skip, for paging
        * Input limit: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        return dicom_connector.get_slides(skip=skip, limit=limit)

    @app.put(
        "/slides/query",
        response_model=ClinicalSlideList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Query slides by case IDs",
    )
    def _(
        slide_query: ClinicalSlideQuery = Body(..., description="Query for filtering the returned items"),
        skip: conint(ge=0) = Query(None, description="Number of cases to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> ClinicalSlideList:
        """
        Get list of filtered Slides, newest first, with paging.
        * Input query: Query for filtering the returned items
        * Input skip: Number of cases to skip, for paging
        * Input limit: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        return dicom_connector.query_slides(slide_query=slide_query, skip=skip, limit=limit)

    @app.get("/slides/{slide_id}/info", response_model=SlideInfo, tags=["WSI Data"])
    def _(slide_id: str, plugin: str = PluginQuery):
        """
        Get metadata information for a slide given its ID
        """
        return dicom_connector.get_slide_info(slide_id=slide_id)

    @app.get(
        "/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["WSI Data"],
    )
    def _(
        slide_id: str,
        max_x: int = Path(
            example=100,
            ge=1,
            le=settings.max_thumbnail_size,
            description="Maximum width of thumbnail",
        ),
        max_y: int = Path(
            example=100,
            ge=1,
            le=settings.max_thumbnail_size,
            description="Maximum height of thumbnail",
        ),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
    ):
        """
        Get slide thumbnail image  given its ID.
        You additionally need to set a maximum width and height for the thumbnail.
        Images will be scaled to match these requirements while keeping the aspect ratio.

        Optionally, the image format and its quality (e.g. for jpeg) can be selected.
        Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        """
        return dicom_connector.get_slide_thumbnail(
            slide_id=slide_id, max_x=max_x, max_y=max_y, image_format=image_format, image_quality=image_quality
        )

    @app.get(
        "/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["WSI Data"],
    )
    def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of label image"),
        max_y: int = Path(example=100, description="Maximum height of label image"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
    ):
        """
        Get the label image of a slide given its ID.
        You additionally need to set a maximum width and height for the label image.
        Images will be scaled to match these requirements while keeping the aspect ratio.

        Optionally, the image format and its quality (e.g. for jpeg) can be selected.
        Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        """
        return dicom_connector.get_slide_label(
            slide_id=slide_id, max_x=max_x, max_y=max_y, image_format=image_format, image_quality=image_quality
        )

    @app.get(
        "/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["WSI Data"],
    )
    def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of macro image"),
        max_y: int = Path(example=100, description="Maximum height of macro image"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
    ):
        """
        Get the macro image of a slide given its ID.
        You additionally need to set a maximum width and height for the macro image.
        Images will be scaled to match these requirements while keeping the aspect ratio.

        Optionally, the image format and its quality (e.g. for jpeg) can be selected.
        Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        """
        return dicom_connector.get_slide_macro(
            slide_id=slide_id, max_x=max_x, max_y=max_y, image_format=image_format, image_quality=image_quality
        )

    @app.get(
        "/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        responses=ImageRegionResponse,
        response_class=StreamingResponse,
        tags=["WSI Data"],
    )
    def _(
        slide_id: str,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="x component of start coordinate of requested region"),
        start_y: int = Path(example=0, description="y component of start coordinate of requested region"),
        size_x: int = Path(gt=0, example=1024, description="Width of requested region"),
        size_y: int = Path(gt=0, example=1024, description="Height of requested region"),
        image_channels: List[int] = ImageChannelQuery,
        z: int = ZStackQuery,
        padding_color: str = ImagePaddingColorQuery,
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
    ):
        """
        Get a region of a slide given its ID and by providing the following parameters:

        * `level` - Pyramid level of the region. Level 0 is highest (original) resolution.
        The available levels depend on the image.

        * `start_x`, `start_y` - Start coordinates of the requested region.
        Coordinates are given with respect to the requested level.
        Coordinates define the upper left corner of the region with respect to the image origin
        (0, 0) at the upper left corner of the image.

        * `size_x`, `size_y` - Width and height of requested region.
        Size needs to be given with respect to the requested level.

        There are a number of addtional query parameters:

        * `image_channels` - Single channels (or multiple channels) can be retrieved through the optional parameter
        image_channels as an integer array referencing the channel IDs.
        This is paricularly important for images with abitrary image channels and channels with a higher
        color depth than 8bit (e.g. fluorescence images).
        The channel composition of the image can be obtained through the slide info endpoint,
        where the dedicated channels are listed along with its color, name and bitness.
        By default all channels are returned.

        * `z` - The region endpoint also offers the selection of a layer in a Z-Stack by setting the index z.
        Default is z=0.

        * `padding_color` - Background color as 24bit-hex-string with leading #,
        that is used when image region contains whitespace when out of image extent. Default is white.
        Only works for 8-bit RGB slides, otherwise the background color is black.

        * `image_format` - The image format can be selected. Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        Multi-channel images can also be represented as RGB-images (mostly for displaying reasons in the viewer).
        Note that the mapping of all color channels to RGB values is currently restricted to the first three channels.
        Default is jpeg.

        * `image_quality` - The image quality can be set for specific formats,
        e.g. for the jpeg format a value between 0 and 100 can be selected. Default is 90.
        """
        return dicom_connector.get_slide_region(
            slide_id=slide_id,
            level=level,
            start_x=start_x,
            start_y=start_y,
            size_x=size_x,
            size_y=size_y,
            image_channels=image_channels,
            z=z,
            padding_color=padding_color,
            image_format=image_format,
            image_quality=image_quality,
        )

    @app.get(
        "/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
        responses=ImageResponses,
        response_class=StreamingResponse,
        tags=["WSI Data"],
    )
    def _(
        slide_id: str,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        tile_x: int = Path(example=0, description="Request the tile_x-th tile in x dimension"),
        tile_y: int = Path(example=0, description="Request the tile_y-th tile in y dimension"),
        image_channels: List[int] = ImageChannelQuery,
        z: int = ZStackQuery,
        padding_color: str = ImagePaddingColorQuery,
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
    ):
        """
        Get a tile of a slide given its ID and by providing the following parameters:

        * `level` - Pyramid level of the tile. Level 0 is highest (original) resolution.
        The available levels depend on the image.

        * `tile_x`, `tile_y` - Coordinates are given with respect to tiles,
        i.e. tile coordinate n is the n-th tile in the respective dimension.
        Coordinates are also given with respect to the requested level.
        Coordinates (0,0) select the tile at the upper left corner of the image.

        There are a number of addtional query parameters:

        * `image_channels` - Single channels (or multiple channels) can be retrieved through
        the optional parameter image_channels as an integer array referencing the channel IDs.
        This is paricularly important for images with abitrary image channels and channels
        with a higher color depth than 8bit (e.g. fluorescence images).
        The channel composition of the image can be obtained through the slide info endpoint,
        where the dedicated channels are listed along with its color, name and bitness.
        By default all channels are returned.

        * `z` - The region endpoint also offers the selection of a layer in a Z-Stack by setting the index z.
        Default is z=0.

        * `padding_color` - Background color as 24bit-hex-string with leading #,
        that is used when image tile contains whitespace when out of image extent. Default is white.
        Only works for 8-bit RGB slides, otherwise the background color is black.

        * `image_format` - The image format can be selected. Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        Multi-channel images can also be represented as RGB-images (mostly for displaying reasons in the viewer).
        Note that the mapping of all color channels to RGB values is currently restricted to the first three channels.
        Default is jpeg.

        * `image_quality` - The image quality can be set for specific formats,
        e.g. for the jpeg format a value between 0 and 100 can be selected. Default is 90.
        It is ignored if raw jpeg tiles are available through a WSI service plugin.
        """
        return dicom_connector.get_slide_tile(
            slide_id=slide_id,
            level=level,
            tile_x=tile_x,
            tile_y=tile_y,
            image_channels=image_channels,
            z=z,
            padding_color=padding_color,
            image_format=image_format,
            image_quality=image_quality,
        )

    @app.get("/slides/{slide_id}/download", tags=["WSI Data"])
    def _(slide_id: str):
        """
        Download raw slide data as zip
        """
        return dicom_connector.download_slide(slide_id=slide_id)
