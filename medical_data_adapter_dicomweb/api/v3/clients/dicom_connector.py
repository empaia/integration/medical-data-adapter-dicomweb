import time
from queue import Empty, Queue
from typing import List

from fastapi.exceptions import HTTPException
from pydantic import UUID4
from starlette.responses import Response
from wsidicom import WsiDicom
from wsidicom.errors import WsiDicomOutOfBoundsError

from medical_data_adapter_dicomweb.models.v3.clinical import (
    Case,
    CaseList,
    ClinicalSlide,
    ClinicalSlideList,
    ClinicalSlideQuery,
)
from medical_data_adapter_dicomweb.models.v3.slide import SlideInfo
from medical_data_adapter_dicomweb.singletons import (
    case_to_study_mapping,
    dicom_client,
    logger,
    series_to_study_mapping,
    settings,
    slide_to_series_mapping,
    wsidicom_cache,
    wsidicom_cache_lock,
    wsidicom_client,
)
from medical_data_adapter_dicomweb.utils.dicom_utils import (
    dicom_to_empaia_case,
    dicom_to_empaia_case_list,
    dicom_to_empaia_clinical_slide,
    dicom_to_empaia_clinical_slide_list,
    dicom_to_empaia_slide_info,
    generate_uuid,
    get_series_instance_uid,
    get_study_instance_uid,
)
from medical_data_adapter_dicomweb.utils.image_utils import (
    make_image_response,
    rgba_to_rgb_with_background_color,
    validate_complete_region_overlap,
    validate_complete_tile_overlap,
    validate_hex_color_string,
    validate_image_channels,
    validate_image_level,
    validate_image_request,
    validate_image_size,
    validate_image_z,
)


def get_wsidicom_slide(study_instance_uid, series_instance_uid):
    queue: Queue = wsidicom_cache.get(series_instance_uid)
    if queue is None:
        wsidicom_slide = WsiDicom.open_web(
            wsidicom_client, study_uid=study_instance_uid, series_uids=series_instance_uid
        )
        return wsidicom_slide

    try:
        wsidicom_slide = queue.get_nowait()
    except Empty:
        wsidicom_slide = WsiDicom.open_web(
            wsidicom_client, study_uid=study_instance_uid, series_uids=series_instance_uid
        )

    return wsidicom_slide


def put_wsidicom_slide(wsidicom_slide, series_instance_uid):
    queue: Queue = wsidicom_cache.get(series_instance_uid)
    if queue is None:
        with wsidicom_cache_lock:
            queue: Queue = wsidicom_cache.get(series_instance_uid)
            if queue is None:
                queue = Queue()
                wsidicom_cache[series_instance_uid] = queue

    queue.put(wsidicom_slide)


def get_mapped_series_instance_uid(slide_id: str):
    if slide_id in slide_to_series_mapping:
        series_instance_uid = slide_to_series_mapping[slide_id]
        return series_instance_uid

    logger.debug("Cache miss in get_mapped_series_instance_uid for slide_id %s", slide_id)

    serieslist = dicom_client.search_for_series()
    for series in serieslist:
        series_instance_uid = get_series_instance_uid(series)
        generated_slide_id = generate_uuid(series_instance_uid)
        slide_to_series_mapping[generated_slide_id] = series_instance_uid

    if slide_id in slide_to_series_mapping:
        series_instance_uid = slide_to_series_mapping[slide_id]
        return series_instance_uid

    raise HTTPException(status_code=404, detail="Not found.")


def get_mapped_study_indstance_uid(case_id):
    if case_id in case_to_study_mapping:
        return case_to_study_mapping[case_id]

    logger.debug("Cache miss in get_mapped_study_indstance_uid for case_id %s", case_id)

    studylist = dicom_client.search_for_studies()
    for study in studylist:
        study_instance_uid = get_study_instance_uid(study)
        generated_case_id = generate_uuid(study_instance_uid)
        case_to_study_mapping[generated_case_id] = study_instance_uid

    if case_id in case_to_study_mapping:
        return case_to_study_mapping[case_id]

    raise HTTPException(status_code=404, detail="Not found.")


def get_cached_study_instance_uid(series_instance_uid):
    if series_instance_uid in series_to_study_mapping:
        return series_to_study_mapping[series_instance_uid]

    logger.debug("Cache miss in get_cached_study_instance_uid for series_instance_uid %s", series_instance_uid)

    serieslist = dicom_client.search_for_series()
    for series in serieslist:
        study_instance_uid = get_study_instance_uid(series)
        current_series_instance_uid = get_series_instance_uid(series)
        series_to_study_mapping[current_series_instance_uid] = study_instance_uid
        logger.debug("New cache mapping from series %s to study %s", series_instance_uid, study_instance_uid)

    if series_instance_uid in series_to_study_mapping:
        return series_to_study_mapping[series_instance_uid]

    raise HTTPException(status_code=404, detail="Not found.")


def get_case(case_id: UUID4, with_slides: bool) -> Case:
    study_intance_uid = get_mapped_study_indstance_uid(case_id=str(case_id))
    studylist = dicom_client.search_for_studies(search_filters={"StudyInstanceUID": study_intance_uid})

    if not studylist:
        raise HTTPException(status_code=404, detail="Not found.")

    study = studylist[0]  # as StudyInstanceUID is unique, caselist can only contain one element

    if not with_slides:
        return dicom_to_empaia_case(study)

    serieslist = dicom_client.search_for_series(search_filters={"StudyInstanceUID": get_study_instance_uid(study)})
    serieslist_metadata = []
    for series in serieslist:
        series_metadata = dicom_client.retrieve_series_metadata(
            study_instance_uid=get_study_instance_uid(series), series_instance_uid=get_series_instance_uid(series)
        )
        serieslist_metadata.append(series_metadata[0])

    return dicom_to_empaia_case(study, serieslist=serieslist_metadata)


def get_cases(with_slides: bool, skip: int, limit: int) -> CaseList:
    item_count = len(dicom_client.search_for_studies())
    studylist = dicom_client.search_for_studies(offset=skip, limit=limit)

    if not studylist:
        return CaseList(item_count=item_count, items=[])

    if not with_slides:
        return dicom_to_empaia_case_list(studylist, item_count)

    serieslist_metadata = []
    for study in studylist:
        serieslist_case = dicom_client.search_for_series(
            search_filters={"StudyInstanceUID": get_study_instance_uid(study)}
        )

        serieslist_metadata_case = []
        for series in serieslist_case:
            series_metadata = dicom_client.retrieve_series_metadata(
                study_instance_uid=get_study_instance_uid(series), series_instance_uid=get_series_instance_uid(series)
            )
            serieslist_metadata_case.append(series_metadata[0])

        serieslist_metadata.append(serieslist_metadata_case)

    return dicom_to_empaia_case_list(studylist, item_count, serieslist_metadata)


def get_slide(slide_id: UUID4) -> ClinicalSlide:
    series_intance_uid = get_mapped_series_instance_uid(slide_id=str(slide_id))
    serieslist = dicom_client.search_for_series(search_filters={"SeriesInstanceUID": series_intance_uid})

    if not serieslist:
        raise HTTPException(status_code=404, detail="Not found.")

    series = serieslist[0]  # as SeriesInstanceUID is unique, slidelist can only contain one element
    series_metadata = dicom_client.retrieve_series_metadata(
        study_instance_uid=get_study_instance_uid(series), series_instance_uid=series_intance_uid
    )
    return dicom_to_empaia_clinical_slide(series_metadata[0])


def get_slides(skip: int, limit: int) -> ClinicalSlideList:
    item_count = len(dicom_client.search_for_series())
    serieslist = dicom_client.search_for_series(offset=skip, limit=limit)
    if not serieslist:
        return ClinicalSlideList(item_count=item_count, items=[])

    serieslist_metadata = []
    for series in serieslist:
        series_metadata = dicom_client.retrieve_series_metadata(
            study_instance_uid=get_study_instance_uid(series), series_instance_uid=get_series_instance_uid(series)
        )
        serieslist_metadata.append(series_metadata[0])

    return dicom_to_empaia_clinical_slide_list(serieslist_metadata, item_count)


def query_slides(slide_query: ClinicalSlideQuery, skip: int, limit: int) -> ClinicalSlideList:
    if slide_query.cases is None:
        return get_slides(skip=skip, limit=limit)

    serieslist = []
    for _, cases in slide_query:
        for case_id in cases:
            study_instance_uid = get_mapped_study_indstance_uid(case_id=str(case_id))
            serieslist.extend(dicom_client.search_for_series(search_filters={"StudyInstanceUID": study_instance_uid}))
    item_count = len(serieslist)

    reduced_serieslist = serieslist
    if skip:
        reduced_serieslist = reduced_serieslist[skip:]
    if limit:
        reduced_serieslist = reduced_serieslist[:limit]

    serieslist_metadata = []
    for series in reduced_serieslist:
        series_metadata = dicom_client.retrieve_series_metadata(
            study_instance_uid=get_study_instance_uid(series), series_instance_uid=get_series_instance_uid(series)
        )
        serieslist_metadata.append(series_metadata[0])

    return dicom_to_empaia_clinical_slide_list(serieslist_metadata, item_count)


def get_slide_info(slide_id: str) -> SlideInfo:
    series_instance_uid = get_mapped_series_instance_uid(slide_id=slide_id)
    study_instance_uid = get_cached_study_instance_uid(series_instance_uid=series_instance_uid)

    series_metadata_all_levels = dicom_client.retrieve_series_metadata(
        study_instance_uid=study_instance_uid, series_instance_uid=series_instance_uid
    )
    return dicom_to_empaia_slide_info(series_metadata_all_levels)


def get_slide_thumbnail(slide_id: str, max_x: int, max_y: int, image_format: str, image_quality: int) -> Response:
    series_instance_uid = get_mapped_series_instance_uid(slide_id=slide_id)
    study_instance_uid = get_cached_study_instance_uid(series_instance_uid=series_instance_uid)
    validate_image_request(image_format, image_quality)

    start_time = time.time()
    wsidicom_slide = get_wsidicom_slide(study_instance_uid=study_instance_uid, series_instance_uid=series_instance_uid)
    thumbnail = wsidicom_slide.read_thumbnail(size=(settings.max_thumbnail_size, settings.max_thumbnail_size))
    put_wsidicom_slide(wsidicom_slide=wsidicom_slide, series_instance_uid=series_instance_uid)
    diff_time = time.time() - start_time
    logger.debug("Fetched thumbnail in seconds: %f", diff_time)

    thumbnail.thumbnail((max_x, max_y))
    return make_image_response(pil_image=thumbnail, image_format=image_format, image_quality=image_quality)


def get_slide_label(slide_id: str, max_x: int, max_y: int, image_format: str, image_quality: int) -> Response:
    # TODO: response does not cover tiff
    series_instance_uid = get_mapped_series_instance_uid(slide_id=slide_id)
    study_instance_uid = get_cached_study_instance_uid(series_instance_uid=series_instance_uid)
    validate_image_request(image_format, image_quality)

    start_time = time.time()
    wsidicom_slide = get_wsidicom_slide(study_instance_uid=study_instance_uid, series_instance_uid=series_instance_uid)
    label = wsidicom_slide.read_label()
    put_wsidicom_slide(wsidicom_slide=wsidicom_slide, series_instance_uid=series_instance_uid)
    diff_time = time.time() - start_time
    logger.debug("Fetched label in seconds: %f", diff_time)

    label.thumbnail((max_x, max_y))
    return make_image_response(pil_image=label, image_format=image_format, image_quality=image_quality)


def get_slide_macro(slide_id: str, max_x: int, max_y: int, image_format: str, image_quality: int) -> Response:
    # TODO: response does not cover tiff
    series_instance_uid = get_mapped_series_instance_uid(slide_id=slide_id)
    study_instance_uid = get_cached_study_instance_uid(series_instance_uid=series_instance_uid)
    validate_image_request(image_format, image_quality)

    start_time = time.time()
    wsidicom_slide = get_wsidicom_slide(study_instance_uid=study_instance_uid, series_instance_uid=series_instance_uid)
    macro = wsidicom_slide.read_overview()
    put_wsidicom_slide(wsidicom_slide=wsidicom_slide, series_instance_uid=series_instance_uid)
    diff_time = time.time() - start_time
    logger.debug("Fetched macro in seconds: %f", diff_time)

    macro.thumbnail((max_x, max_y))
    return make_image_response(pil_image=macro, image_format=image_format, image_quality=image_quality)


def get_slide_region(
    slide_id: str,
    level: int,
    start_x: int,
    start_y: int,
    size_x: int,
    size_y: int,
    image_channels: List[int],
    z: int,
    padding_color: str,
    image_format: str,
    image_quality: int,
) -> Response:
    series_instance_uid = get_mapped_series_instance_uid(slide_id=slide_id)
    study_instance_uid = get_cached_study_instance_uid(series_instance_uid=series_instance_uid)
    validate_image_request(image_format, image_quality)
    validate_image_size(size_x, size_y)
    slide_info = get_slide_info(slide_id)
    validate_image_level(slide_info, level)
    validate_image_channels(slide_info, image_channels)
    validate_image_z(slide_info, z)
    validate_complete_region_overlap(slide_info, level, start_x, start_y, size_x, size_y)

    start_time = time.time()
    wsidicom_slide = get_wsidicom_slide(study_instance_uid=study_instance_uid, series_instance_uid=series_instance_uid)
    region = wsidicom_slide.read_region(location=(start_x, start_y), level=level, size=(size_x, size_y))
    put_wsidicom_slide(wsidicom_slide=wsidicom_slide, series_instance_uid=series_instance_uid)
    diff_time = time.time() - start_time
    logger.debug("Fetched region in seconds: %f", diff_time)

    return make_image_response(pil_image=region, image_format=image_format, image_quality=image_quality)


def get_slide_tile(
    slide_id: str,
    level: int,
    tile_x: int,
    tile_y: int,
    image_channels: List[int],
    z: int,
    padding_color: str,
    image_format: str,
    image_quality: int,
) -> Response:
    series_instance_uid = get_mapped_series_instance_uid(slide_id=slide_id)
    study_instance_uid = get_cached_study_instance_uid(series_instance_uid=series_instance_uid)
    validate_image_request(image_format, image_quality)
    slide_info = get_slide_info(slide_id)
    validate_image_level(slide_info, level)
    validate_image_channels(slide_info, image_channels)
    validate_image_z(slide_info, z)
    validate_complete_tile_overlap(slide_info, level, tile_x, tile_y)
    vp_color = validate_hex_color_string(padding_color)

    start_time = time.time()
    wsidicom_slide = get_wsidicom_slide(study_instance_uid=study_instance_uid, series_instance_uid=series_instance_uid)
    try:
        rgba_tile = wsidicom_slide.read_tile(level=level, tile=(tile_x, tile_y))
    except WsiDicomOutOfBoundsError as e:
        raise HTTPException(status_code=404, detail="Tile not found.") from e
    put_wsidicom_slide(wsidicom_slide=wsidicom_slide, series_instance_uid=series_instance_uid)
    diff_time = time.time() - start_time
    logger.debug("Fetched tile in seconds: %f", diff_time)

    rgb_tile = rgba_to_rgb_with_background_color(image_rgba=rgba_tile, padding_color=vp_color)
    return make_image_response(pil_image=rgb_tile, image_format=image_format, image_quality=image_quality)


def download_slide(slide_id: str):
    raise HTTPException(status_code=501, detail="Not implemented")
