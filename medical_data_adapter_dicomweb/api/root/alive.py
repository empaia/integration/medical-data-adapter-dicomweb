from fastapi import Response

from ... import __version__ as version
from ...models.commons import ServiceStatus, ServiceStatusEnum


def add_routes_alive(app):
    @app.get(
        "/alive",
        tags=["server"],
        responses={
            200: {"model": ServiceStatus},
            500: {"model": ServiceStatus},
        },
    )
    async def _(
        response: Response,
    ):
        # TODO: proper check if pacs is running and can be reached
        return ServiceStatus(status=ServiceStatusEnum.OK.value, version=version)
