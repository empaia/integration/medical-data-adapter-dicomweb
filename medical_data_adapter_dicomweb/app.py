"""
This modules defines the REST API for the Medical Data Adapter DICOMweb.
"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from . import __version__
from .api.root import add_routes_root
from .api.v3 import add_routes_v3
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

app = FastAPI(
    title="Medical Data Adapter DICOMweb",
    version=__version__,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
)

add_routes_root(app, settings)

app_v3 = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app, app_v3]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )

add_routes_v3(app_v3, settings)
app.mount("/v3", app_v3)
