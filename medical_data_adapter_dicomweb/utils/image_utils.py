import re
from io import BytesIO
from typing import List

from fastapi import HTTPException
from PIL import Image
from starlette.responses import Response

from medical_data_adapter_dicomweb.models.v3.slide import SlideInfo
from medical_data_adapter_dicomweb.singletons import settings

supported_image_formats = {
    "bmp": "image/bmp",
    "gif": "image/gif",
    "jpeg": "image/jpeg",
    "png": "image/png",
    "tiff": "image/tiff",
}

alternative_spellings = {"jpg": "jpeg", "tif": "tiff"}


def save_rgb_image(pil_image, image_format, image_quality) -> BytesIO:
    mem = BytesIO()
    pil_image.save(mem, format=image_format, quality=image_quality)
    mem.seek(0)
    return mem


def make_image_response(pil_image: Image, image_format: str, image_quality: int) -> Response:
    if image_format.lower() in alternative_spellings:
        image_format = alternative_spellings[image_format]

    if image_format == "tiff":
        raise HTTPException(status_code=400, detail="Tiff format not supported.")

    try:
        mem = save_rgb_image(pil_image, image_format, image_quality)
    except ValueError as e:
        raise HTTPException(status_code=404, detail="Tile not found") from e

    return Response(mem.getvalue(), media_type=supported_image_formats[image_format])


def rgba_to_rgb_with_background_color(image_rgba: Image, padding_color: str) -> Image:
    if image_rgba.info.get("transparency", None) is not None or image_rgba.mode == "RGBA":
        image_rgb = Image.new("RGB", image_rgba.size, padding_color)
        image_rgb.paste(image_rgba, mask=image_rgba.split()[3])
    else:
        image_rgb = image_rgba.convert("RGB")
    return image_rgb


def validate_image_request(image_format: str, image_quality: int) -> None:
    if image_format not in supported_image_formats and image_format not in alternative_spellings:
        raise HTTPException(status_code=400, detail="Provided image format parameter not supported")
    if image_quality < 0 or image_quality > 100:
        raise HTTPException(status_code=400, detail="Provided image quality parameter not supported")


def validate_image_size(size_x: int, size_y: int) -> None:
    if size_x * size_y > settings.max_returned_region_size:
        raise HTTPException(
            status_code=422,
            detail=f"Requested region may not contain more than {settings.max_returned_region_size} pixels.",
        )


def validate_image_level(slide_info: SlideInfo, level: int) -> None:
    if level >= len(slide_info.levels):
        raise HTTPException(
            status_code=422,
            detail="The requested pyramid level is not available. "
            + f"The coarsest available level is {len(slide_info.levels) - 1}.",
        )


def validate_image_channels(slide_info: SlideInfo, image_channels: List[int]) -> None:
    if image_channels is None:
        return None
    for i in image_channels:
        if i >= len(slide_info.channels):
            raise HTTPException(
                status_code=400,
                detail=f"""
                Selected image channel exceeds channel bounds
                (selected: {i} max: {len(slide_info.channels)-1})
                """,
            )
    if len(image_channels) != len(set(image_channels)):
        raise HTTPException(status_code=400, detail="No duplicates allowed in channels")


def validate_image_z(slide_info: SlideInfo, z: int) -> None:
    if z > 0 and (slide_info.extent.z == 1 or slide_info.extent.z is None):
        raise HTTPException(
            status_code=422,
            detail=f"Invalid ZStackQuery z={z}. The image does not support multiple z-layers.",
        )
    if z > 0 and z >= slide_info.extent.z:
        raise HTTPException(
            status_code=422,
            detail=f"Invalid ZStackQuery z={z}. The image has only {slide_info.extent.z} z-layers.",
        )


def validate_hex_color_string(padding_color: str) -> str:
    if padding_color:
        match = re.search(r"^#(?:[0-9a-fA-F]{3}){1,2}$", padding_color)
        if match:
            stripped_padding_color = padding_color.lstrip("#")
            int_padding_color = tuple(int(stripped_padding_color[i : i + 2], 16) for i in (0, 2, 4))
            return int_padding_color
    return settings.padding_color


def validate_complete_tile_overlap(slide_info: SlideInfo, level: int, tile_x: int, tile_y: int) -> None:
    tile_count_x = int(slide_info.levels[level].extent.x / slide_info.tile_extent.x)
    tile_count_y = int(slide_info.levels[level].extent.y / slide_info.tile_extent.y)
    if not tile_x >= 0 and tile_y >= 0 and tile_x < tile_count_x and tile_y < tile_count_y:
        raise HTTPException(
            status_code=422,
            detail=f"Invalid tile position. The image has only {slide_info.tile_count_x} tiles in x "
            + f"and {slide_info.tile_count_y} tiles in y direction.",
        )


def validate_complete_region_overlap(
    slide_info: SlideInfo, level: int, start_x: int, start_y: int, size_x: int, size_y: int
) -> None:
    if not (
        start_x >= 0
        and start_y >= 0
        and start_x + size_x < slide_info.levels[level].extent.x
        and start_y + size_y < slide_info.levels[level].extent.y
    ):
        raise HTTPException(
            status_code=422,
            detail=f"Invalid region size. The image has only size {slide_info.levels[level].extent.x} x "
            + f"{slide_info.levels[level].extent.y} pixels.",
        )
