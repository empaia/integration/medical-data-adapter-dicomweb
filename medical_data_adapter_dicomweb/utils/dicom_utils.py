import hashlib
import uuid
from datetime import datetime
from typing import Dict, List, Optional

from pydicom import Dataset

from medical_data_adapter_dicomweb.models.v3.clinical import Case, CaseList, ClinicalSlide, ClinicalSlideList
from medical_data_adapter_dicomweb.models.v3.slide import (
    SlideChannel,
    SlideColor,
    SlideExtent,
    SlideInfo,
    SlideLevel,
    SlidePixelSizeNm,
)
from medical_data_adapter_dicomweb.singletons import logger, tags_dict


def generate_uuid(instance_uid):
    instance_hash = hashlib.md5(instance_uid.encode("utf-8"))
    instance_int = int.from_bytes(instance_hash.digest(), byteorder="little")
    instance_uuid = uuid.UUID(int=instance_int, version=4)
    return str(instance_uuid)


def get_series_instance_uid(dcm_json: Dict) -> str:
    return dcm_json["0020000E"]["Value"][0]


def get_study_instance_uid(dcm_json: Dict) -> str:
    return dcm_json["0020000D"]["Value"][0]


def get_image_type(dcm_json: Dict) -> str:
    return dcm_json["00080008"]["Value"][2]


def match_tissue(dcm_tag):
    dcm_tag = dcm_tag.casefold()
    for key, val in tags_dict["TISSUE"].items():
        if dcm_tag in val["EN"].casefold():
            return key
    return None


def match_stain(dcm_tag):
    dcm_tag = dcm_tag.casefold()
    if "hematoxylin" in dcm_tag and "eosin" in dcm_tag:
        return "H_AND_E"
    return None


def get_tissue(series: Dataset) -> Optional[str]:
    try:
        dcm_tissue = series.SpecimenDescriptionSequence[0].PrimaryAnatomicStructureSequence[0].CodeMeaning
    except Exception as e:
        logger.debug(e)
        return None

    return match_tissue(dcm_tissue)


def get_stain(series: Dataset) -> Optional[str]:
    staining_steps = []

    try:
        specimen_prep_seq = series.SpecimenDescriptionSequence[0].SpecimenPreparationSequence
    except Exception as e:
        logger.debug(e)
        return None

    for ds in specimen_prep_seq:
        try:
            seq = ds.SpecimenPreparationStepContentItemSequence
        except Exception as e:
            logger.debug(e)
            continue

        using_substance_start_index = None
        for i, ds_a in enumerate(seq):
            try:
                concept = ds_a.ConceptCodeSequence[0]
                concept_name = ds_a.ConceptNameCodeSequence[0]
            except Exception as e:
                logger.debug(e)
                continue

            if not (concept_name.CodeValue == "111701" and concept_name.CodingSchemeDesignator == "DCM"):
                continue

            if not (concept.CodeValue == "127790008" and concept.CodingSchemeDesignator == "SCT"):
                continue

            # Found sequence start of "staining"
            using_substance_start_index = i
            break

        if using_substance_start_index is None:
            continue

        for ds_a in seq[using_substance_start_index:]:
            try:
                concept = ds_a.ConceptCodeSequence[0]
                concept_name = ds_a.ConceptNameCodeSequence[0]
            except Exception as e:
                logger.debug(e)
                continue

            if not (concept_name.CodeValue == "424361007" and concept_name.CodingSchemeDesignator == "SCT"):
                continue

            # Found "using substance"
            staining_steps.append(concept.CodeMeaning)

    if not staining_steps:
        return None

    dcm_tag = " ".join(staining_steps)
    return match_stain(dcm_tag)


def get_block(series: Dataset) -> str:
    try:
        specimen_prep_seq = series.SpecimenDescriptionSequence[0].SpecimenPreparationSequence
    except Exception as e:
        logger.debug(e)
        return None

    for ds in specimen_prep_seq:
        try:
            seq = ds.SpecimenPreparationStepContentItemSequence
        except Exception as e:
            logger.debug(e)
            continue

        block_creation_start_index = None
        for i, ds_a in enumerate(seq):
            try:
                concept_name = ds_a.ConceptNameCodeSequence[0]
            except Exception as e:
                logger.debug(e)
                continue

            if not (concept_name.CodeValue == "111703" and concept_name.CodingSchemeDesignator == "DCM"):
                continue

            try:
                text_value = ds.TextValue
            except Exception as e:
                logger.debug(e)
                continue

            if not (text_value == "Block creation"):
                continue

            # Found sequence start of "Block creation"
            block_creation_start_index = i
            break

        if block_creation_start_index is None:
            continue

        for ds_a in seq[block_creation_start_index:]:
            try:
                concept_name = ds_a.ConceptNameCodeSequence[0]
            except Exception as e:
                logger.debug(e)
                continue

            if not (concept_name.CodeValue == "121041" and concept_name.CodingSchemeDesignator == "DCM"):
                continue

            try:
                block = ds.TextValue
            except Exception as e:
                logger.debug(e)
                continue

            # Found "Specimen Identifier"
            return block

    return None


def get_study_timestamp(ds: Dataset):
    study_date = None
    try:
        study_date = ds.StudyDate
    except Exception as e:
        logger.debug(e)

    if not study_date:
        study_date = "19700101"

    study_time = None
    try:
        study_time = ds.StudyTime.split(".")[0]
    except Exception as e:
        logger.debug(e)

    if not study_time:
        study_time = "000000"

    study_date_time = study_date + study_time
    study_datetime = datetime.strptime(study_date_time, "%Y%m%d%H%M%S")
    return study_datetime.timestamp()


def get_acquistion_timestamp(ds: Dataset):
    acquisition_date_time = None
    try:
        acquisition_date_time = ds.AcquisitionDateTime.split(".")[0]
    except Exception as e:
        logger.debug(e)

    if not acquisition_date_time:
        acquisition_date_time = "19700101000000"

    acquisition_datetime = datetime.strptime(acquisition_date_time, "%Y%m%d%H%M%S")
    return acquisition_datetime.timestamp()


def dicom_to_empaia_clinical_slide(series: Dict) -> ClinicalSlide:
    ds = Dataset.from_json(series)

    parameter_dict = {}
    parameter_dict["case_id"] = generate_uuid(ds.StudyInstanceUID)
    parameter_dict["tissue"] = get_tissue(ds)
    parameter_dict["stain"] = get_stain(ds)
    parameter_dict["block"] = get_block(ds)
    parameter_dict["created_at"] = get_acquistion_timestamp(ds)
    parameter_dict["updated_at"] = parameter_dict["created_at"]
    parameter_dict["id"] = generate_uuid(ds.SeriesInstanceUID)

    return ClinicalSlide.model_validate(parameter_dict)


def dicom_to_empaia_clinical_slide_list(serieslist: List[Dict], item_count: int) -> ClinicalSlideList:
    clinical_slides = [dicom_to_empaia_clinical_slide(slide) for slide in serieslist]
    return ClinicalSlideList(item_count=item_count, items=clinical_slides)


def dicom_to_empaia_case(study: Dict, serieslist: List[Dict] = None) -> Case:
    ds = Dataset.from_json(study)

    parameter_dict = {}
    parameter_dict["creator_id"] = "USER_ID"
    parameter_dict["creator_type"] = "USER"
    parameter_dict["created_at"] = get_study_timestamp(ds)
    parameter_dict["updated_at"] = parameter_dict["created_at"]
    parameter_dict["description"] = ds.StudyID or None
    parameter_dict["id"] = generate_uuid(ds.StudyInstanceUID)

    if serieslist:
        parameter_dict["slides"] = [dicom_to_empaia_clinical_slide(series) for series in serieslist]
        return Case.model_validate(parameter_dict)
    else:
        return Case.model_validate(parameter_dict)


def dicom_to_empaia_case_list(studylist: List[Dict], item_count: int, serieslist: List[List[Dict]] = None) -> CaseList:
    if serieslist:
        cases = [dicom_to_empaia_case(study, series) for study, series in zip(studylist, serieslist)]
    else:
        cases = [dicom_to_empaia_case(study) for study in studylist]
    return CaseList(item_count=item_count, items=cases)


def get_channels(metadata: Dataset) -> List[SlideChannel]:
    rgb_image_channels = [
        SlideChannel(id=0, name="Red", color=SlideColor(r=255, g=0, b=0, a=0)),
        SlideChannel(id=0, name="Green", color=SlideColor(r=0, g=255, b=0, a=0)),
        SlideChannel(id=0, name="Blue", color=SlideColor(r=0, g=0, b=255, a=0)),
    ]
    if metadata.PhotometricInterpretation == "RGB":
        return rgb_image_channels
    else:
        return None


def sort_by_pixel_spacing(levels: List[Dataset]) -> float:
    levels.sort(key=lambda x: float(x.SharedFunctionalGroupsSequence[0].PixelMeasuresSequence[0].PixelSpacing[0]))
    return None


def compute_pixel_size_nm(level: Dataset) -> SlidePixelSizeNm:
    (pixel_spacing_x, pixel_spacing_y) = level.SharedFunctionalGroupsSequence[0].PixelMeasuresSequence[0].PixelSpacing
    return SlidePixelSizeNm(
        x=pixel_spacing_x * 1e6,  # given in mm, converted to nm
        y=pixel_spacing_y * 1e6,  # given in mm, converted to nm
    )


def get_extent(level: Dataset, base_level_x: int) -> SlideLevel:
    return SlideLevel(
        extent=SlideExtent(
            x=level.TotalPixelMatrixRows, y=level.TotalPixelMatrixColumns, z=level.TotalPixelMatrixFocalPlanes
        ),
        downsample_factor=(base_level_x / level.TotalPixelMatrixRows),
    )


def dicom_to_empaia_slide_info(series: List[Dict]) -> SlideInfo:
    levels = [
        Dataset.from_json(level) for level in series if get_image_type(level) == "VOLUME"
    ]  # exclude macro and label images here
    sort_by_pixel_spacing(levels)
    base_level = levels[0]

    parameter_dict = {}
    parameter_dict["id"] = generate_uuid(base_level.SeriesInstanceUID)
    parameter_dict["extent"] = {
        "x": base_level.TotalPixelMatrixRows,
        "y": base_level.TotalPixelMatrixColumns,
        "z": base_level.TotalPixelMatrixFocalPlanes,
    }
    parameter_dict["num_levels"] = len(levels)
    parameter_dict["pixel_size_nm"] = compute_pixel_size_nm(base_level)
    parameter_dict["tile_extent"] = {
        "x": base_level.Rows,
        "y": base_level.Columns,
        "z": base_level.TotalPixelMatrixFocalPlanes,
    }
    parameter_dict["levels"] = [get_extent(level, base_level_x=base_level.TotalPixelMatrixRows) for level in levels]
    parameter_dict["channels"] = get_channels(base_level)
    parameter_dict["channel_depth"] = base_level.BitsStored
    parameter_dict["format"] = "medical-data-adapter-dicomweb-DCM"
    parameter_dict["raw_download"] = False

    return SlideInfo.model_validate(parameter_dict)
