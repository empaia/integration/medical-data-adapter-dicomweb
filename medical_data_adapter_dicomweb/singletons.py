import csv
import logging
from pathlib import Path
from threading import Lock

import wsidicom
from dicomweb_client import DICOMwebClient

from .settings import Settings

settings = Settings()

dicom_webserver_url = settings.dicom_webserver_url.rstrip("/")
dicom_client = DICOMwebClient(url=f"{dicom_webserver_url}/rs")
wsidicom_client = wsidicom.WsiDicomWebClient(dicom_client)
case_to_study_mapping = {}
slide_to_series_mapping = {}
series_to_study_mapping = {}
wsidicom_cache = {}
wsidicom_cache_lock = Lock()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

# read available Tags from Definitions
_tags_csv = Path(__file__).parent / "definitions/tags/tags.csv"
tags_dict = {}
with open(_tags_csv, encoding="utf-8") as f:
    for tag in csv.DictReader(f, delimiter=";"):
        grp = tag.pop("TAG_GROUP")
        id_ = tag.pop("TAG_NAME_ID")
        tags_dict.setdefault(grp, {})[id_] = tag
