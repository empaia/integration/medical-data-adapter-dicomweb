FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.7@sha256:c0ba63ee114a51de4aae83a43532668437ae4e76e674bc961e3ec5d72d06c9f1 AS builder
COPY . /mdad
WORKDIR /mdad
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.7@sha256:09d2bc7198a5cb03bdd3605f17dc160495e2c442a1d3619b1b7c0cd3dc72c827
COPY --from=builder /mdad/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /mdad/dist /artifacts
RUN pip install /artifacts/*.whl
COPY ./run.sh /opt/app/bin/run.sh
