import requests

from .. import TIMEOUT
from ..singletons import mdad_url

CASE_IDS = ["b21fac7d-726e-46a6-b99b-bb384dca17fc", "e472abdf-714f-4199-ae3b-c399530e859a"]
SLIDE_IDS = ["2a606781-39bc-4687-a9b6-85473b46f508", "a8d840fe-f27f-4bcf-a106-d6ac39ff6506"]


def test_get_cases():
    r = requests.get(f"{mdad_url}/v3/cases", timeout=TIMEOUT)
    r.raise_for_status()
    cases = r.json()
    assert len(cases["items"]) == cases["item_count"]

    case_ids = [case["id"] for case in cases["items"]]
    case_ids.sort()
    assert case_ids == CASE_IDS


def test_get_cases_with_slides():
    r = requests.get(f"{mdad_url}/v3/cases", params={"with_slides": True}, timeout=TIMEOUT)
    r.raise_for_status()
    cases = r.json()

    for case in cases["items"]:
        assert len(case["slides"]) == 1


def test_get_case():
    for case_id in CASE_IDS:
        r = requests.get(f"{mdad_url}/v3/cases/{case_id}", timeout=TIMEOUT)
        r.raise_for_status()
        case = r.json()
        assert case_id == case["id"]


def test_get_case_with_slides():
    for case_id in CASE_IDS:
        r = requests.get(f"{mdad_url}/v3/cases/{case_id}", params={"with_slides": True}, timeout=TIMEOUT)
        r.raise_for_status()
        case = r.json()
        assert case_id == case["id"]
        assert len(case["slides"]) == 1


def test_get_slides():
    r = requests.get(f"{mdad_url}/v3/slides", timeout=TIMEOUT)
    r.raise_for_status()
    slides = r.json()

    slide_ids = [slide["id"] for slide in slides["items"]]
    slide_ids.sort()
    assert slide_ids == SLIDE_IDS


def test_get_slide():
    for slide_id in SLIDE_IDS:
        r = requests.get(f"{mdad_url}/v3/slides/{slide_id}", timeout=TIMEOUT)
        r.raise_for_status()
        slide = r.json()
        assert slide_id == slide["id"]


def test_slides_query():
    query = {"cases": CASE_IDS}
    r = requests.put(f"{mdad_url}/v3/slides/query", json=query, timeout=TIMEOUT)
    r.raise_for_status()
    slides = r.json()
    case_ids = [slide["case_id"] for slide in slides["items"]]
    case_ids.sort()
    assert case_ids == CASE_IDS

    query = {"cases": CASE_IDS[:1]}
    r = requests.put(f"{mdad_url}/v3/slides/query", json=query, timeout=TIMEOUT)
    r.raise_for_status()
    slides = r.json()
    case_ids = [slide["case_id"] for slide in slides["items"]]
    case_ids.sort()
    assert case_ids == CASE_IDS[:1]

    query = {"cases": CASE_IDS[1:]}
    r = requests.put(f"{mdad_url}/v3/slides/query", json=query, timeout=TIMEOUT)
    r.raise_for_status()
    slides = r.json()
    case_ids = [slide["case_id"] for slide in slides["items"]]
    case_ids.sort()
    assert case_ids == CASE_IDS[1:]

    query = {"cases": None}
    r = requests.put(f"{mdad_url}/v3/slides/query", json=query, timeout=TIMEOUT)
    r.raise_for_status()
    slides = r.json()
    case_ids = [slide["case_id"] for slide in slides["items"]]
    case_ids.sort()
    assert case_ids == CASE_IDS

    query = {}
    r = requests.put(f"{mdad_url}/v3/slides/query", json=query, timeout=TIMEOUT)
    r.raise_for_status()
    slides = r.json()
    case_ids = [slide["case_id"] for slide in slides["items"]]
    case_ids.sort()
    assert case_ids == CASE_IDS


def test_get_slide_info():
    for slide_id in SLIDE_IDS:
        print(slide_id)
        r = requests.get(f"{mdad_url}/v3/slides/{slide_id}/info", timeout=TIMEOUT)
        r.raise_for_status()
        slide_info = r.json()
        assert slide_id == slide_info["id"]
        assert not slide_info["raw_download"]


def test_get_slide_thumbnail():
    max_x, max_y = 256, 256
    for slide_id in SLIDE_IDS:
        r = requests.get(f"{mdad_url}/v3/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}", timeout=TIMEOUT)
        r.raise_for_status()
        thumbnail = r.content
        assert len(thumbnail) > 100


def test_get_slide_macro():
    max_x, max_y = 256, 256
    for slide_id in SLIDE_IDS:
        r = requests.get(f"{mdad_url}/v3/slides/{slide_id}/macro/max_size/{max_x}/{max_y}", timeout=TIMEOUT)
        r.raise_for_status()
        macro = r.content
        assert len(macro) > 100


def test_get_slide_label():
    max_x, max_y = 256, 256
    for slide_id in SLIDE_IDS:
        r = requests.get(f"{mdad_url}/v3/slides/{slide_id}/label/max_size/{max_x}/{max_y}", timeout=TIMEOUT)
        r.raise_for_status()
        label = r.content
        assert len(label) > 100


def test_get_slide_region():
    level, start_x, start_y, size_x, size_y = 0, 0, 0, 256, 256
    for slide_id in SLIDE_IDS:
        r = requests.get(
            f"{mdad_url}/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
            timeout=TIMEOUT,
        )
        r.raise_for_status()
        region = r.content
        assert len(region) > 100


def test_get_slide_tile():
    level, tile_x, tile_y = 0, 0, 0
    for slide_id in SLIDE_IDS:
        r = requests.get(f"{mdad_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}", timeout=TIMEOUT)
        print(r.text)
        r.raise_for_status()
        tile = r.content
        assert len(tile) > 100
