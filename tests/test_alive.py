import requests

from medical_data_adapter_dicomweb.models.commons import ServiceStatus, ServiceStatusEnum

from . import TIMEOUT
from .singletons import mdad_url


def test_alive():
    response = requests.get(f"{mdad_url}/alive", timeout=TIMEOUT)
    assert response.status_code == 200
    status = ServiceStatus.model_validate(response.json())
    assert status.status == ServiceStatusEnum.OK
