from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    mdad_url: str
    model_config = SettingsConfigDict(env_file=".env", env_prefix="PYTEST_")
